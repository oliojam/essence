﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constants;

public class Level : MonoBehaviour
{
    public NodeGrid nodeGrid;
    public Node nodePrefab;
    public Link linkPrefab;
    public GameObject nodeContainer;
    public GameObject linkContainer;
    public Node heartNode;
    public Camera mainCamera;
    public Player player;
    public SpriteRenderer blackOverlay;
    public AudioSource audioPower1;
    public AudioSource audioPower2;
    public AudioSource audioPower3;
    public AudioSource audioLevelComplete;
    public AudioSource enterSound;

    public int numNodes = 20;
    public int numPowerNodes = 3;
    public bool isComplete = false;
    public float startZoom = 5.0f;
    public float maxZoom = 8.05f;
    public float zoomRatio = 0.0f;
    float overlayRatio = 1.0f;

    List<Node> nodes;
    List<Link> links;
    List<Node> powerNodes;

    void Start()
    {
        CreateNodes();
        LinkNodes();
        CreatePowerNodes();
        enterSound.Play();
    }

    void Clear()
    {
    	foreach (Node node in nodes)
    	{
    		Destroy(node.gameObject);
    	}

    	foreach (Link link in links)
    	{
    		Destroy(link.gameObject);
    	}

    	nodeGrid.Clear();
    	nodes.Clear();
    	links.Clear();
    	isComplete = false;
    	zoomRatio = 0.0f;
    }

    void Update()
    {
    	bool readyToComplete = true;

    	// check if all power nodes have been pulsed
    	foreach (Node powerNode in powerNodes)
    	{
    		if (!powerNode.HasBeenPulsed())
    		{
    			readyToComplete = false;
    			break;
    		}
    	}

    	if (isComplete != readyToComplete)
    	{
    		CompleteLevel();
    	}

    	if (isComplete && Input.GetMouseButton(0))
    	{
    		float delta = Time.deltaTime * 1.0f;
        	overlayRatio = Mathf.Clamp(overlayRatio + delta, 0.0f, 1.0f);
    	}
    	else
    	{
    		float delta = Time.deltaTime * 1.0f;
        	overlayRatio = Mathf.Clamp(overlayRatio - delta, 0.0f, 1.0f);
    	}

    	// black overlay
    	Color overlayColor = blackOverlay.color;
    	overlayColor.a = overlayRatio;
    	blackOverlay.color = overlayColor;

    	// Zoom
    	if (isComplete)
    	{
    		float delta = Time.deltaTime * 0.02f;
        	zoomRatio = Mathf.Clamp(zoomRatio + delta, 0.0f, 1.0f);
        }
		mainCamera.orthographicSize = (maxZoom - startZoom) * zoomRatio + startZoom;

		// Restart level when fully zoomed out
    	if (isComplete && overlayRatio == 1.0f)
    	{
    		Clear();
    		Start();
    	}

    	if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    void CompleteLevel()
    {
    	isComplete = true;

    	// Play level complete sound
    	audioPower1.Play();
    	audioPower2.Play();
    	audioPower3.Play();
    	audioLevelComplete.Play();

    	foreach (Node node in nodes)
    	{
    		node.gameObject.SetActive(false);
    	}

    	foreach (Link link in links)
    	{
    		link.isLevelComplete = true;
    	}
    }

    void CreateNodes()
    {
        nodes = new List<Node>();

        // add heart node
        heartNode = Instantiate(nodePrefab, nodeContainer.transform);
        heartNode.transform.position = nodeGrid.GetHeartPosition();
        heartNode.SetType(NodeType.Heart);
        heartNode.owningPlayer = player;
        nodes.Add(heartNode);

        // add other nodes
        for (int i = 0; i < numNodes; i++)
        {
            Vector3 position = nodeGrid.GetRandomPosition();
            Node node = Instantiate(nodePrefab, position, Quaternion.identity, nodeContainer.transform);
            nodes.Add(node);
        }
    }

    void LinkNodes()
    {
        links = new List<Link>();

        foreach(Node node in nodes)
        {
            // loop through nodes to check if close enough to form link 
            foreach(Node otherNode in nodes)
            {
                // not self
                if (node != otherNode)
                {
                    // check if already exists
                    bool isExisting = false;
                    foreach (Link link in links)
                    {
                        if (link.Equals(node, otherNode))
                        {
                            isExisting = true;
                        }
                    }

                    float linkRange = nodeGrid.cellSize * 1.5f;

                    // not existing and inside range
                    if (!isExisting && Vector3.Distance(node.transform.position, otherNode.transform.position) < linkRange)
                    {
                        Link link = Instantiate(linkPrefab, linkContainer.transform);
                        link.Init(node, otherNode);
                        node.AddLink(link);
                        otherNode.AddLink(link);
                        links.Add(link);
                    }
                }
            }
        }
    }

    void OnPowerPulse(Node node)
    {
    	if (!isComplete && !node.HasBeenPulsed())
    	{
	    	if (powerNodes[0] == node)
	    	{
				audioPower1.Play();
	    	}
	    	else if (powerNodes[1] == node)
			{
				audioPower2.Play();
			}
			else if (powerNodes[2] == node)
			{
				audioPower3.Play();
			}
		}
    }

    void CreatePowerNodes()
    {
    	powerNodes = new List<Node>();

        for (int i = 0; i < numPowerNodes; i++)
        {
            Node node = null;
            int attempts = 0;
        	int maxAttempts = 200;

            do
            {
            	attempts += 1;
                node = nodes[Random.Range(0, nodes.Count)];

                // Do not allow power nodes on or connected to the heartNode
                // Do not allowe power nodes without any links
                if (node == heartNode || node.IsConnectedTo(heartNode) || !node.HasConnections())
                {
                	node = null;
                }

                // Do not let same nodes be chosen
                foreach (Node otherNode in powerNodes)
                {
                	if (node == otherNode)
                	{
                		node = null;
                	}
                }
            }
            while (node == null && attempts < maxAttempts);

            if (attempts >= maxAttempts)
	        {
	            Debug.LogError("No space for power node.");
	        }

	        node.OnPowerPulse += OnPowerPulse;

            node.SetType(NodeType.Power);
            powerNodes.Add(node);
        }
    }
}
