﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour
{
	Node startNode;
	Node endNode;
	float pulseRatio;
	int index;

	public void Init(Node start, Node end, int index)
	{
		startNode = start;
		endNode = end;
		this.index = index;
		pulseRatio = 0.0f;
		transform.position = startNode.transform.position;
	}

    void Update()
    {
    	if (startNode == null || endNode == null)
    	{
    		Destroy(gameObject);
    		return;
    	}

    	float delta = Time.deltaTime * 2.0f;
    	pulseRatio = Mathf.Clamp(pulseRatio + delta, 0.0f, 1.0f);

    	Vector3 startPos = startNode.transform.position;
    	Vector3 endPos = endNode.transform.position;
    	transform.position = startPos + (endPos - startPos) * pulseRatio;

    	if (pulseRatio == 1.0f)
    	{
    		endNode.OnPulse(startNode, index);
    		Destroy(gameObject);
    	}
    }
}
