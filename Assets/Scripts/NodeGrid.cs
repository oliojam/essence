﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeGrid : MonoBehaviour
{
    public int xCells;
    public int yCells;
    public float cellSize;

    Dictionary<Vector3, bool> cellDict;

    public Vector3 getGridSize()
    {
      return new Vector3(xCells * cellSize - cellSize, yCells * cellSize - cellSize);
    }

    void Awake()
    {
        Clear();
    }

    public void Clear()
    {
        cellDict = new Dictionary<Vector3,bool>();
    }

    public Vector3 GetHeartPosition()
    {
        // center heart in grid
        int x = xCells/2;
        int y = yCells/2;

        Vector3 cellPos = new Vector3(x,y);
        cellDict.Add(cellPos, true);

        return CellToPosition(cellPos);
    }

    public Vector3 GetRandomPosition()
    {
        Vector3 cellPos;
        int attempts = 0;
        int maxAttempts = 200;

        do
        {
            attempts += 1;
            int x = Random.Range(0, xCells);
            int y = Random.Range(0, yCells);
            cellPos = new Vector3(x,y);
        }
        while (cellDict.ContainsKey(cellPos) && attempts < maxAttempts);

        if (attempts >= maxAttempts)
        {
            Debug.LogError("No empty cell to place node.");
        }

        cellDict.Add(cellPos, true);

        return CellToPosition(cellPos);
    }

    public Vector3 CellToPosition(Vector3 cellPos)
    {
        Vector3 gridSize = getGridSize();
        return new Vector3(cellPos.x * cellSize - gridSize.x/2, cellPos.y * cellSize - gridSize.y/2);
    }
}
