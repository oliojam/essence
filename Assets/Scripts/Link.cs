﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Link : MonoBehaviour
{
	public Pulse pulsePrefab;
    public SpriteRenderer spriteRenderer;
    public GameObject flow;

    public float flowRatio = 0.0f;
    public float alphaRatio = 0.0f;
    public Node targetNode;
    public bool isLevelComplete = false;
    public bool isBonded = false;

    List<Node> nodes;

    public void Init(Node node1, Node node2)
    {
      nodes = new List<Node>() {node1, node2};
    }

    public bool Equals(Node node1, Node node2)
    {
      return ((nodes[0] == node1 && nodes[1] == node2) ||
          (nodes[0] == node2 && nodes[1] == node1));
    }

    public bool IsConnectedToCapturedNode()
    {
    	foreach (Node node in nodes)
	    {
	    	if (node.IsCaptured())
	    	{
	    		return true;
	    	}
	    }

	    return false;
    }

    public Node GetLinked(Node node)
    {
        if (node == nodes[0])
        {
            return nodes[1];
        }
        else if (node == nodes[1])
        {
            return nodes[0];
        }
        else
        {
            Debug.LogWarning("node does exist along link");
            return null;
        }
    }

    void Start()
    {
        // stretch the sprite to connect the nodes
        float spriteSize = spriteRenderer.sprite.rect.width / spriteRenderer.sprite.pixelsPerUnit;

        Vector3 centerPos = (nodes[1].transform.position + nodes[0].transform.position) / 2.0f;
        transform.position = centerPos;

        Vector3 direction = nodes[1].transform.position - nodes[0].transform.position;
        direction = Vector3.Normalize(direction);
        transform.right = direction;

        Vector3 scale = new Vector3(1,1,1);
        scale.x = Vector3.Distance(nodes[0].transform.position, nodes[1].transform.position)/ spriteSize;
        transform.localScale = scale;
    }

    void Update()
    {
        if (!isBonded)
        {
            if (targetNode)
            {
                Node linkedNode = GetLinked(targetNode);
                Vector3 direction = targetNode.transform.position - linkedNode.transform.position;
                direction = Vector3.Normalize(direction);
                flow.transform.right = direction;

                flow.transform.position = linkedNode.transform.position;
            }

            float speed = Time.deltaTime * 2.0f;
            float delta = targetNode ? speed: -speed ;
            flowRatio = Mathf.Clamp(flowRatio + delta, 0.0f, 1.0f);
        }

        if (isLevelComplete)
        {
        	float delta = Time.deltaTime * 4.0f;
            alphaRatio = Mathf.Clamp(alphaRatio - delta, 0.0f, 1.0f);
        }
        else if (IsConnectedToCapturedNode())
        {
            float delta =  Time.deltaTime * 4.0f;
            alphaRatio = Mathf.Clamp(alphaRatio + delta, 0.0f, 1.0f);
        }

        Vector3 scale = new Vector3(1,1,1);
        scale.x = flowRatio;
        flow.transform.localScale = scale;

        // Hide base link
        Color color = spriteRenderer.color;

       	color.a = alphaRatio - flowRatio;
        spriteRenderer.color = color;
    }

    public void PulseFrom(Node startNode, int index)
    {
    	Node endNode = GetLinked(startNode);
    	Pulse pulse = Instantiate(pulsePrefab);
    	pulse.Init(startNode, endNode, index);
    }
}
