using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Constants;
using System;

namespace Constants
{
   public enum NodeType
    {
        Free,
        Captured,
        Heart,
        Power,
        CapturedPower
    } 
}

public class Node : MonoBehaviour
{
	public NodeType nodeType = NodeType.Free;

	public GameObject nodeIcon;
    public GameObject nodeIconOver;
	public GameObject capturedIcon;
	public GameObject heartIcon;
    public GameObject powerIcon;
    public GameObject capturedPowerIcon;
    public Ping nodePingPrefab;
    public Ping heartPingPrefab;
    public Ping powerNodePingPrefab;
    public AudioSource tickSound;
    public AudioSource heartPulseSound;

    public event Action<Node> OnPowerPulse;
    
    public Player owningPlayer;
    public float timeBetweenPulses = 4.0f;

    GameObject icon;
    List<Link> links = new List<Link>();
    bool isOver = false;
    bool isDown = false;
    float overRatio = 0.0f;
    List<Link> flowLinks;
    int pulseIndex = -1;
    float pulseRatio = 0;

    void Start()
    {
        flowLinks = new List<Link>();
        SetType(nodeType);
    }

    public void AddLink(Link link)
    {
    	links.Add(link);
    }

    public bool CanAcceptPulse(int index)
    {
        return index > pulseIndex;
    }

    public bool IsCaptured()
    {
        return nodeType == NodeType.Heart ||
            nodeType == NodeType.Captured ||
            nodeType == NodeType.CapturedPower;
    }

    public bool HasBeenPulsed()
    {
        return pulseIndex > 0;
    }

    public bool HasConnections()
    {
        return links.Count > 0;
    }

    void OnMouseOver()
    {
        isOver = true;
    }

    void OnMouseExit()
    {
        isOver = false;
    }

    void OnMouseDown()
    {
        isDown = true;
    }

    void OnMouseUp()
    {
        isDown = false;
    }

    bool CanBeCaptured()
    {
    	if (nodeType == NodeType.Free || nodeType == NodeType.Power)
    	{
	        foreach (Link link in links)
	        {
	            Node linkedNode = link.GetLinked(this);
	            if (linkedNode.owningPlayer)
	            {
	                return true;
	            }
	        }
    	}

        return false;
    }

    public bool IsConnectedTo(Node node)
    {
        foreach (Link link in links)
        {
            Node linkedNode = link.GetLinked(this);
            if (linkedNode == node)
            {
                return true;
            }
        }

        return false;
    }

    bool IsVisible()
    {
        if (nodeType == NodeType.Free || nodeType == NodeType.Power)
        {
            return CanBeCaptured();
        }
        else
        {
            return true;
        }
    }

    public void SetType(NodeType type)
    {
    	nodeType = type;
    	switch(nodeType)
    	{
    		case NodeType.Free:
    			icon = nodeIcon;
    			break;
    		case NodeType.Captured:
    			icon = capturedIcon;
    			break;
    		case NodeType.Heart:
    			icon = heartIcon;
                break;
            case NodeType.Power:
                icon = powerIcon;
                break;
            case NodeType.CapturedPower:
                icon = capturedPowerIcon;
    			break;
    	}

    	// disable all icons
    	nodeIcon.SetActive(false);
        nodeIconOver.SetActive(false);
    	capturedIcon.SetActive(false);
    	heartIcon.SetActive(false);
        powerIcon.SetActive(false);
        capturedPowerIcon.SetActive(false);

    	// enable only active icon
    	icon.SetActive(true);
    }

    void Update()
    {
        if (isDown && (nodeType == NodeType.Free || nodeType == NodeType.Power))
        {
            nodeIconOver.SetActive(true);

            foreach (Link link in links)
            {
                Node linkedNode = link.GetLinked(this);
                if (linkedNode.owningPlayer && !flowLinks.Contains(link))
                {
                    link.targetNode = this;
                    flowLinks.Add(link);
                }
            }
        }
        else
        {
            nodeIconOver.SetActive(false);

            foreach (Link flowLink in flowLinks)
            {
                flowLink.targetNode = null;
            }
            flowLinks.Clear();
        }

        foreach (Link flowLink in flowLinks)
        {
            if (flowLink.flowRatio == 1.0f)
            {
                Player player = flowLink.GetLinked(this).owningPlayer;
                Capture(player);
                flowLink.isBonded = true;
            }
        }

        // grow on over
        float speed = Time.deltaTime * 5.0f;
        float delta = isOver && CanBeCaptured() ? speed: -speed ;
        overRatio = Mathf.Clamp(overRatio + delta, 0.0f, 1.0f);
        float scale = 1 + overRatio;
        transform.localScale = new Vector3(scale, scale);

        // heart sends out pulses
        if (nodeType == NodeType.Heart)
        {
        	float pulseDelta = Time.deltaTime/timeBetweenPulses;
        	pulseRatio = Mathf.Clamp(pulseRatio + pulseDelta, 0.0f, 1.0f);

        	if (pulseRatio == 1)
        	{
        		OnPulse(null, pulseIndex + 1);
        		pulseRatio = 0;
        	}
        }

        icon.SetActive(IsVisible());
    }

    void Capture(Player player)
    {
        owningPlayer = player;
        tickSound.Play();

        switch(nodeType)
        {
            case NodeType.Free:
                SetType(NodeType.Captured);
                break;
            case NodeType.Power:
                SetType(NodeType.CapturedPower);
                break;
        }
    }

    void playPowerSound()
    {
        if (OnPowerPulse != null)
        {
            OnPowerPulse(this);
        }
    }

    void playHeartSound()
    {
        heartPulseSound.Play();
    }

    public void OnPulse(Node fromNode, int index)
    {
    	if (index > pulseIndex) 
		{
            Ping ping;
            switch(nodeType)
            {
                case NodeType.Heart:
                    ping = heartPingPrefab;
                    playHeartSound();
                    break;
                case NodeType.Power:
                case NodeType.CapturedPower:
                    ping = powerNodePingPrefab;
                    playPowerSound();
                    break;
                default:
                    ping = nodePingPrefab;
                    break;
            }
            Instantiate(ping, this.transform);
            pulseIndex = index;

	    	foreach (Link link in links)
		    {
                Node linkedNode = link.GetLinked(this);

		    	if (linkedNode.CanAcceptPulse(index) && link.isBonded)
		    	{
		    		link.PulseFrom(this, index);
		    	}
		    }
		}
    }
}
