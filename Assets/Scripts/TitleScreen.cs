﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : MonoBehaviour
{
    public SpriteRenderer titleImage;

    float titleRatio = 1.0f;
    bool isClicked = false;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
           isClicked = true;
        }

        if (isClicked)
        {
            float delta = Time.deltaTime * 1.0f;
            titleRatio = Mathf.Clamp(titleRatio - delta, 0.0f, 1.0f);

            Color color = titleImage.color;
            color.a = titleRatio;
            titleImage.color = color;

            if (titleRatio == 0.0f)
            {
                Destroy(gameObject);
            }
        }
    }
}
